<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MalasngodingEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('syaiful9507@gmail.com')
                    ->view('emailku')
                    ->with(
                        [
                            'nama'  => 'SYAIFUL',
                            'website' => 'www.syaifulsmart.tech',
                        ])
                        ->attach(public_path('/data_upload').'/BIODATA ZAA 2020.pdf', [
                            'as'    => 'BIODATA ZAA 2020.pdf',
                            'mime'  => 'doc/pdf',
                        ]);
    }
}
