<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gambar;
use File;

class UploadController extends Controller
{
    public function upload() {
        $gambar = Gambar::get();
        return view('upload', ['gambar' => $gambar]);
    }

    public function proses_upload(Request $request) {
        $this->validate($request, [
            'file'  => 'required | file | mimes:jpeg,png,jpg|max:2048',
            'keterangan' => 'required',
        ]);

        //menyiapkan data file yang di upload ke variable $file

        $file = $request->file('file');

        //nama file
        $nama_file = time()."_".$file->getClientOriginalName();

    

        //isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_upload';

        //upload file
        $file->move($tujuan_upload, $nama_file);

        Gambar::create([
            'file' => $nama_file,
            'keterangan' => $request->keterangan,
        ]);

        return redirect()->back();
    }

    public function hapus($id)
    {
        $gambar = Gambar::where('id',$id)->first();
        File::delete('data_upload/'.$gambar->file);

        //hapus data
        Gambar::where('id',$id)->delete();
        return redirect()->back();

    }
}
