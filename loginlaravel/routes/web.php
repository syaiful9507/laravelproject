<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/enkripsi', 'SyaifulController@enkripsi');
Route::get('/data', 'SyaifulController@data');

Route::get('/hash', 'SyaifulController@hash');

Route::get('/upload','UploadController@upload');
Route::post('/upload/proses','UploadController@proses_upload');

//Hapus File
Route::get('/upload/hapus/{id}','UploadController@hapus');

//Session
Route::get('/session/tampil','TestController@tampilkanSession');
Route::get('/session/buat','TestController@buatSession');
Route::get('/session/hapus','TestController@hapusSession');

//notification

Route::get('/pesan','NotifController@index');
Route::get('/pesan/sukses','NotifController@sukses');
Route::get('/pesan/peringatan','NotifController@peringatan');
Route::get('/pesan/gagal','NotifController@gagal');


//kirim email
Route::get('/kirimemail','MalasngodingController@index');
