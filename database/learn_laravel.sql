-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 11, 2020 at 06:49 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `learn_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Sari Tania Puspita', NULL, NULL),
(2, 'Diki Alfarabi Hadi', NULL, NULL),
(3, 'Luluh Sinaga', NULL, NULL),
(4, 'Lamar Putra', NULL, NULL),
(5, 'Banawi Kuswoyo', NULL, NULL),
(6, 'Ratih Wijayanti', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `anggota_hadiah`
--

CREATE TABLE `anggota_hadiah` (
  `id` int(10) UNSIGNED NOT NULL,
  `anggota_id` int(10) UNSIGNED NOT NULL,
  `hadiah_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anggota_hadiah`
--

INSERT INTO `anggota_hadiah` (`id`, `anggota_id`, `hadiah_id`, `created_at`, `updated_at`) VALUES
(1, 6, 6, NULL, NULL),
(2, 2, 5, NULL, NULL),
(3, 6, 10, NULL, NULL),
(4, 3, 4, NULL, NULL),
(5, 3, 6, NULL, NULL),
(6, 1, 4, NULL, NULL),
(7, 4, 11, NULL, NULL),
(8, 5, 5, NULL, NULL),
(9, 2, 9, NULL, NULL),
(10, 6, 6, NULL, NULL),
(11, 3, 2, NULL, NULL),
(12, 2, 3, NULL, NULL),
(13, 1, 8, NULL, NULL),
(14, 6, 8, NULL, NULL),
(15, 3, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `judul`, `created_at`, `updated_at`) VALUES
(1, 'Placeat saepe ea possimus provident quos est molestiae reiciendis.', NULL, NULL),
(2, 'Totam laudantium molestiae similique sit.', NULL, NULL),
(3, 'Aut consequatur ducimus ut non voluptatem voluptas.', NULL, NULL),
(4, 'Ad sit voluptatem qui ut dolorem.', NULL, NULL),
(5, 'Qui consequatur eum fuga corrupti.', NULL, NULL),
(6, 'Quos nesciunt blanditiis amet odio.', NULL, NULL),
(7, 'Ex doloremque consequuntur velit alias repellendus ullam.', NULL, NULL),
(8, 'Perspiciatis a quo beatae nobis et suscipit illo.', NULL, NULL),
(9, 'Maiores voluptate animi est enim totam.', NULL, NULL),
(10, 'Rerum expedita inventore nulla voluptates perferendis placeat.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `guru_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `umur` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`guru_id`, `nama`, `umur`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Fitria Gilda Hastuti S.Pd', 40, NULL, '2020-06-02 20:41:20', NULL),
(4, 'Cemeti Gunarto', 33, NULL, '2020-06-02 20:41:34', NULL),
(5, 'Gangsa Samosir', 41, NULL, '2020-06-02 20:41:03', NULL),
(8, 'Dartono Kusumo', 36, NULL, '2020-06-02 20:50:52', NULL),
(9, 'Hardi Asmadi Uwais S.Ked', 26, NULL, '2020-06-02 20:50:56', NULL),
(10, 'Gina Halimah', 39, NULL, '2020-06-02 20:50:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hadiah`
--

CREATE TABLE `hadiah` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_hadiah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hadiah`
--

INSERT INTO `hadiah` (`id`, `nama_hadiah`, `created_at`, `updated_at`) VALUES
(1, 'Kulkas', NULL, NULL),
(2, 'Lemari', NULL, NULL),
(3, 'Rumah', NULL, NULL),
(4, 'Mobil', NULL, NULL),
(5, 'Sepeda Motor', NULL, NULL),
(6, 'Pulpen', NULL, NULL),
(7, 'Tas', NULL, NULL),
(8, 'Sepatu', NULL, NULL),
(9, 'Voucher', NULL, NULL),
(10, 'Mouse', NULL, NULL),
(11, 'Laptop', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `pegawai_id` int(11) NOT NULL,
  `pegawai_nama` varchar(50) NOT NULL,
  `pegawai_jabatan` varchar(20) NOT NULL,
  `pegawai_umur` int(11) NOT NULL,
  `pegawai_alamat` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`pegawai_id`, `pegawai_nama`, `pegawai_jabatan`, `pegawai_umur`, `pegawai_alamat`, `created_at`, `updated_at`) VALUES
(1, 'Prof. Dr. Syaiful, S.T, M.T', 'Direktur Utama', 27, 'DKI JAKARTA, Sumenep, Bogor', '2020-06-03 02:14:55', '2020-06-02 19:14:55'),
(2, 'Wulan Indah Safitri', 'Komisaris', 24, 'DKI Jakarta, Gresik', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(3, 'Bukarna', 'Komisaris', 50, 'Sumenep', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(6, 'Syaiful', 'Software Engineer', 25, 'Jl. Dki Jakarta', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(8, 'Purwadi Situmorang', 'animi', 34, 'Gg. Wahidin Sudirohusodo No. 824, Tidore Kepulauan 89554, Jambi', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(9, 'Harimurti Muhammad Situmorang', 'voluptatem', 26, 'Dk. Kyai Gede No. 473, Banjarmasin 10346, BaBel', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(10, 'Karman Putra', 'omnis', 33, 'Dk. Jaksa No. 502, Pariaman 87203, PapBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(13, 'Dian Winda Hastuti S.I.Kom', 'doloremque', 35, 'Ki. Labu No. 331, Pematangsiantar 13790, KepR', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(14, 'Kamaria Lailasari', 'deserunt', 31, 'Kpg. Labu No. 160, Lubuklinggau 28452, Jambi', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(15, 'Omar Pradipta', 'et', 27, 'Ki. Flora No. 548, Bontang 17857, KepR', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(16, 'Yuliana Handayani', 'aut', 37, 'Psr. Villa No. 943, Cimahi 67935, KalSel', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(17, 'Shania Hani Suartini', 'assumenda', 37, 'Kpg. Babakan No. 302, Padangpanjang 53948, JaBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(18, 'Ivan Firmansyah', 'voluptates', 30, 'Dk. Abdullah No. 226, Medan 93199, Lampung', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(19, 'Patricia Namaga', 'quae', 38, 'Gg. Basoka Raya No. 737, Depok 65554, KalSel', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(20, 'Jono Narpati', 'error', 27, 'Psr. Bappenas No. 727, Tarakan 41749, DIY', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(21, 'Tirta Nugroho', 'adipisci', 28, 'Ki. Padang No. 469, Surabaya 57386, JaTeng', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(22, 'Puspa Palastri', 'quo', 40, 'Kpg. Bakin No. 308, Bekasi 16119, Jambi', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(23, 'Laswi Bagas Dongoran', 'similique', 40, 'Dk. Untung Suropati No. 160, Tanjungbalai 15887, Jambi', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(24, 'Oliva Oni Mardhiyah M.Kom.', 'dolor', 27, 'Gg. Teuku Umar No. 870, Pekanbaru 37379, KalTeng', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(25, 'Pranata Maryadi', 'consequatur', 36, 'Ds. Baladewa No. 407, Semarang 85630, PapBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(26, 'Ajeng Widya Melani', 'deleniti', 28, 'Ki. Sumpah Pemuda No. 786, Surakarta 27319, SumSel', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(27, 'Yahya Mansur', 'sequi', 40, 'Jln. Cikutra Timur No. 373, Batam 38192, SumBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(28, 'Elvin Rajasa', 'dolorem', 26, 'Jln. Baing No. 174, Metro 55366, SulTeng', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(29, 'Bambang Sihombing M.M.', 'asperiores', 34, 'Dk. Sadang Serang No. 403, Tangerang Selatan 68967, JaBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(30, 'Ghaliyati Yuniar M.M.', 'unde', 31, 'Dk. Kali No. 356, Bukittinggi 13556, KalSel', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(31, 'Jayeng Irnanto Zulkarnain', 'aut', 29, 'Psr. Suniaraja No. 597, Administrasi Jakarta Selatan 39103, JaTeng', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(32, 'Harsaya Jailani', 'ullam', 34, 'Kpg. Kiaracondong No. 798, Dumai 99952, NTT', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(33, 'Puspa Hamima Hariyah S.T.', 'culpa', 35, 'Gg. Pacuan Kuda No. 402, Salatiga 19497, SumUt', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(34, 'Dono Saefullah M.TI.', 'consectetur', 26, 'Kpg. Yos Sudarso No. 426, Bogor 74368, SulUt', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(35, 'Jaiman Gunawan', 'sit', 31, 'Ds. Baja No. 917, Bima 34397, SulUt', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(36, 'Laswi Ardianto M.M.', 'ipsum', 34, 'Dk. Sunaryo No. 597, Yogyakarta 99204, Banten', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(37, 'Hamzah Mulya Sihombing M.Pd', 'et', 38, 'Ki. Flora No. 765, Subulussalam 28307, KalTim', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(38, 'Himawan Waluyo', 'reprehenderit', 32, 'Ds. Ronggowarsito No. 405, Banda Aceh 37186, JaTim', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(39, 'Jaeman Ajimat Nugroho M.Pd', 'sequi', 31, 'Kpg. Bara Tambar No. 147, Ternate 81560, JaBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(40, 'Vera Utami', 'consequuntur', 29, 'Gg. Moch. Toha No. 466, Salatiga 44974, KalBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(41, 'Aisyah Novi Wulandari S.Sos', 'impedit', 30, 'Dk. Bahagia No. 595, Medan 23651, BaBel', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(42, 'Eka Bakidin Mandala S.E.', 'voluptas', 26, 'Ki. Urip Sumoharjo No. 891, Payakumbuh 64692, PapBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(43, 'Endra Lamar Natsir M.Pd', 'dolorem', 30, 'Jr. Lembong No. 499, Bogor 40330, SulTra', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(44, 'Gandewa Iswahyudi', 'facilis', 30, 'Kpg. Jayawijaya No. 76, Mojokerto 42615, DKI', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(45, 'Yono Putra', 'a', 35, 'Kpg. Madiun No. 622, Medan 40158, Banten', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(46, 'Radit Waluyo', 'alias', 32, 'Kpg. BKR No. 12, Depok 51332, Bali', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(47, 'Candrakanta Ridwan Wijaya S.E.I', 'ab', 31, 'Jr. Babadan No. 270, Kupang 85976, Lampung', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(48, 'Vivi Riyanti', 'facere', 32, 'Ki. Bata Putih No. 418, Cilegon 51335, DKI', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(49, 'Cinthia Queen Aryani', 'repellat', 35, 'Gg. Bata Putih No. 938, Tasikmalaya 82304, MalUt', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(50, 'Cawuk Narpati', 'aspernatur', 39, 'Kpg. Kusmanto No. 748, Palu 40763, Bali', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(51, 'Unjani Hesti Laksmiwati S.Pt', 'ab', 37, 'Ds. Madiun No. 620, Tual 30095, JaBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(52, 'Teguh Mustofa', 'vel', 38, 'Psr. Villa No. 111, Jayapura 17286, KalSel', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(53, 'Siska Ulva Nasyiah', 'laborum', 27, 'Jln. Haji No. 592, Balikpapan 88882, Banten', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(54, 'Yuliana Nabila Purwanti', 'sint', 26, 'Ki. Baladewa No. 266, Tegal 73493, KepR', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(55, 'Calista Palastri', 'aut', 29, 'Gg. Pasteur No. 10, Lubuklinggau 47825, Riau', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(56, 'Kenari Pranowo S.Kom', 'iusto', 34, 'Gg. Achmad No. 641, Jayapura 92410, JaBar', '2020-06-02 05:59:18', '0000-00-00 00:00:00'),
(57, 'Samaniya', 'Direksi', 45, 'Sumenep', '2020-06-01 22:59:51', '2020-06-01 22:59:51'),
(58, 'MatKajin', 'Staff', 50, 'Dasuk', '2020-06-01 23:48:00', '2020-06-01 23:48:00'),
(59, 'Mai', 'Dirut utama', 60, 'Sumenep', '2020-06-03 02:14:06', '2020-06-02 19:14:06');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Padmi Kamila Hassanah S.Farm', NULL, NULL),
(2, 'Julia Oktaviani', NULL, NULL),
(3, 'Darmana Sitompul', NULL, NULL),
(4, 'Naradi Nainggolan', NULL, NULL),
(5, 'Tedi Winarno', NULL, NULL),
(6, 'Ulya Yani Permata S.Pt', NULL, NULL),
(7, 'Maida Uyainah', NULL, NULL),
(8, 'Putri Dian Nasyidah M.Pd', NULL, NULL),
(9, 'Lantar Uwais', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag`, `article_id`, `created_at`, `updated_at`) VALUES
(1, 'dolores', 2, NULL, NULL),
(2, 'culpa', 3, NULL, NULL),
(3, 'sit', 4, NULL, NULL),
(4, 'quasi', 3, NULL, NULL),
(5, 'inventore', 5, NULL, NULL),
(6, 'ut', 7, NULL, NULL),
(7, 'quisquam', 5, NULL, NULL),
(8, 'fugiat', 7, NULL, NULL),
(9, 'perspiciatis', 5, NULL, NULL),
(10, 'voluptatem', 3, NULL, NULL),
(11, 'non', 2, NULL, NULL),
(12, 'ducimus', 5, NULL, NULL),
(13, 'tempora', 4, NULL, NULL),
(14, 'voluptatem', 10, NULL, NULL),
(15, 'nisi', 3, NULL, NULL),
(16, 'exercitationem', 10, NULL, NULL),
(17, 'sed', 2, NULL, NULL),
(18, 'tempora', 6, NULL, NULL),
(19, 'laudantium', 7, NULL, NULL),
(20, 'a', 6, NULL, NULL),
(21, 'consequuntur', 9, NULL, NULL),
(22, 'omnis', 1, NULL, NULL),
(23, 'rerum', 9, NULL, NULL),
(24, 'ut', 1, NULL, NULL),
(25, 'amet', 10, NULL, NULL),
(26, 'atque', 9, NULL, NULL),
(27, 'at', 4, NULL, NULL),
(28, 'hic', 3, NULL, NULL),
(29, 'itaque', 1, NULL, NULL),
(30, 'quia', 9, NULL, NULL),
(31, 'consequatur', 8, NULL, NULL),
(32, 'non', 9, NULL, NULL),
(33, 'explicabo', 10, NULL, NULL),
(34, 'eos', 6, NULL, NULL),
(35, 'eveniet', 5, NULL, NULL),
(36, 'dolor', 10, NULL, NULL),
(37, 'magnam', 10, NULL, NULL),
(38, 'aut', 1, NULL, NULL),
(39, 'et', 6, NULL, NULL),
(40, 'ut', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `telepon`
--

CREATE TABLE `telepon` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor_telepon` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengguna_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `telepon`
--

INSERT INTO `telepon` (`id`, `nomor_telepon`, `pengguna_id`, `created_at`, `updated_at`) VALUES
(1, '(+62) 878 0989 834', 1, NULL, NULL),
(2, '(+62) 509 9868 0557', 2, NULL, NULL),
(3, '023 9503 4379', 3, NULL, NULL),
(4, '(+62) 24 1120 052', 4, NULL, NULL),
(5, '0535 3676 2454', 5, NULL, NULL),
(6, '0614 0945 4128', 6, NULL, NULL),
(7, '0460 8541 5478', 7, NULL, NULL),
(8, '(+62) 713 5497 976', 8, NULL, NULL),
(9, '(+62) 653 4057 294', 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'SYAIFUL', 'syaiful@gmail.com', NULL, '$2y$10$QLcEjwMbAljFKfr8Q73nf.KlJC2yANDXZGiL/un2uMpyqBRfFHLFu', NULL, '2020-06-07 01:07:40', '2020-06-07 01:07:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anggota_hadiah`
--
ALTER TABLE `anggota_hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`guru_id`);

--
-- Indexes for table `hadiah`
--
ALTER TABLE `hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`pegawai_id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telepon`
--
ALTER TABLE `telepon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `anggota_hadiah`
--
ALTER TABLE `anggota_hadiah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `guru_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `hadiah`
--
ALTER TABLE `hadiah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `pegawai_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `telepon`
--
ALTER TABLE `telepon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
